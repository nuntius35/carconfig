import requests
import zipfile
import io
import hashlib
from timeit import default_timer as timer


def get_data(data_url, checksum):
    print('[get_data] Downloading data from web.....',
              end='', flush=True)
    start = timer()
    r = requests.get(data_url)
    end = timer()
    print('[{:7.3f} s]'.format(end-start))
    print('[get_data] Checking integrity............',
              end='', flush=True)
    sha256 = hashlib.sha256(io.BytesIO(r.content).read()).hexdigest()
    end = timer()
    print('[{:7.3f} s]'.format(end-start))
    if checksum == sha256:
        print('[get_data] Unzipping.....................',
                  end='', flush=True)
        start = timer()
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall()
        end = timer()
        print('[{:7.3f} s]'.format(end-start))
    else:
        print('[get_data] Error checking data integrity.')

if __name__ == '__main__':
    data_url = 'https://em.iism.kit.edu/ag-dank2015/resources/ag_dank_2015-dataset2_v2.zip'
    checksum = '120387fe5b5f61ad1d56c29ace7d1f5a024dac2864ae7068ebbf053df0b24028'
    get_data(data_url, checksum)
