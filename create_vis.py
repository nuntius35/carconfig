import csv


def create_vis(filename):
    def vis_line(row, i, f):
        engine = row[1]
        line = int(row[2])-1
        boxes = ["\myboxfst", "\myboxscd", "\myboxthd", "\myboxfth"]
        box = boxes[line]
        quant = ["25", "50", "75", "100"]
        chroma = quant[int(row[43])-1]

        for j in range(7, 43):
            config = ""
            if row[j] == "1":
                config = "1"
                tikz = box + "{(" + str(j-7) + "," + str(-i) + ")}{mycolor" + engine + "}{" + chroma + "}{" + config + "}"
                print(tikz, file=f)


    with open(filename) as csvfile:
        data = csv.reader(csvfile, delimiter=',')
        for row in data:
            old_price = int(row[0])
            break
        nprice = 0
        line = 0
        for i, row in enumerate(data):
            fname = "tikz-code-" + str(nprice) + ".tex"
            f = open(fname, 'a')
            price = int(row[0])
            if price == old_price:
                vis_line(row, line, f)
                line += 1
            else:
                old_price = price
                nprice += 1
                line = 0
                f.close()
                fname = "tikz-code-" + str(nprice) + ".tex"
                f = open(fname, 'a')
                vis_line(row, line, f)
                line += 1


if __name__ == '__main__':
    filename = 'results.csv'
    create_vis(filename)
