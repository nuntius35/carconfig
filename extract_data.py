from timeit import default_timer as timer


def extract_data(filename):
    fout = open(filename, 'w')
    fin = 'ag_dank_2015-dataset2/konfigurator_small.dat'

    print('[extract_data] Extracting data...........',
              end='', flush=True)
    start = timer()
    with open(fin) as file:
        for line in file:
            if line[17] != '0':
                print(line[4:10] + ',' +
                    line[10] + ',' +
                    line[17] + ',' +
                    line[18:20] + ',' +
                    line[20:22] + ',' +
                    line[22:24] + ',' +
                    line[24:26] + ',' +
                    ','.join(line[i] for i in range(53,89))+';', file = fout)
    end = timer()
    print('[{:7.3f} s]'.format(end-start))
            #if line[26]!='0':
                #print(line[10]+','+line[26]+','+line[27:29]+','+line[29:31]+','+line[31:33]+','+line[33:35]+','+','.join(line[i] for i in range(89,125))+';')
            #if line[35]!='0':
                #print(line[10]+','+line[35]+','+line[36:38]+','+line[38:40]+','+line[40:42]+','+line[42:44]+','+','.join(line[i] for i in range(125,161))+';')
            #if line[44]!='0':
                #print(line[10]+','+line[44]+','+line[45:47]+','+line[47:49]+','+line[49:51]+','+line[51:53]+','+','.join(line[i] for i in range(161,197))+';')


if __name__ == '__main__':
    filename = 'data.csv'
    extract_data(filename)
