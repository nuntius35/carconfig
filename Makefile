all: vis.pdf

data:
	python3 get_data.py

ag_dank_2015-dataset2/konfigurator_small.dat:
	python3 get_data.py

data.csv: ag_dank_2015-dataset2/konfigurator_small.dat
	python3 extract_data.py

results.csv: data.csv
	octave process.m

tikz-code-*.tex: results.csv
	python3 create_vis.py

vis.pdf: vis.tex tikz-code-*.tex
	latexmk -quiet -lualatex -pdflua

vis.png: vis.pdf
	convert -density 450 -trim vis.pdf -quality 100 vis.png

cleanall:
	rm -rf ag_dank_2015-dataset2
	rm -f data.csv
	rm -f tikz-code*.tex
	latexmk -C -pdf

.PHONY: cleanall data
