# Process the car configuration data
# Output the unique configuartions and their frequency quartile

fprintf('[Octave] Process configuration data......');
fflush(stdout);
tic();
A = csvread('data.csv');
# Remove data with price 0
A = A(A(:,1)!=0,:);
[B,I,J] = unique(A,'rows');
[~,f,~] = unique(sort(J));
freq = diff([0;f]);
q = quantile(freq);
f1 = freq <= q(2);
f2 = freq <= q(3);
f3 = freq <= q(4);
f4 = freq <= q(5);
fq = f1 + 2*f2.*(1-f1) + 3*f3.*(1-f2) + 4*f4.*(1-f3);
B = [B,fq];
csvwrite('results.csv',B)
t = toc();
fprintf('[%7.3f s]\n',t);
fflush(stdout);
