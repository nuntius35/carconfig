# Visualization of car configuration data

This projects visualizes configuration data of an online car configurator.
On three consecutive days, more than 450000 cars were configured, more than 900 of these configurations were unique.
Each configuration consists of a line, an engine and 36 binary attributes.
Other options such as color, rim, seats, ... are not shown in the image.

## Requirements

I am using Linux with

* Python 3.7.2
* Octave 5.1.0
* LuaTeX 1.07.0

To run the analysis, type `make`.

The data was provided by TNS Infratest and can be downloaded [here](https://em.iism.kit.edu/ag-dank2015/resources/ag_dank_2015-dataset2_v2.zip).

---
![Visualization](vis.png)
---

[Homepage of Andreas Geyer-Schulz](https://nuntius35.gitlab.io)
